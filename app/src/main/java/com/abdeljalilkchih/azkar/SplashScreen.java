package com.abdeljalilkchih.azkar;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import static android.os.SystemClock.sleep;

public class SplashScreen extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGTH = 3000;
    Thread splashTread;
    ImageView imageView;
    TextView textView;
    public static AsyncTask mBackgroundTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorSplashScreen));
        }

       // StartAnimations();
        handleActivity();
    }
//    private void StartAnimations() {
//        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
//        anim.reset();
//        LinearLayout l=(LinearLayout) findViewById(R.id.lin_lay);
//        l.clearAnimation();
//        l.startAnimation(anim);
//
//        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
//        anim.reset();
//        ImageView iv = (ImageView) findViewById(R.id.img_logo);
//        iv.clearAnimation();
//        iv.startAnimation(anim);
//
//
//        splashTread = new Thread() {
//            @Override
//            public void run() {
//                try {
//                    int waited = 0;
//                    // Splash screen pause time
//                    while (waited < 3500) {
//                        sleep(100);
//                        waited += 100;
//                    }
//
//
//                    startActivity();
//
//                } catch (InterruptedException e) {
//                    // do nothing
//                } finally {
//                    SplashScreen.this.finish();
//                }
//
//            }
//        };
//        splashTread.start();
//
//    }

    public  void startActivity(){



        Intent intent = new Intent(SplashScreen.this, MainActivity.class);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {

            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(SplashScreen.this);
            startActivity(intent,options.toBundle());

        }else {
            startActivity(intent);
        }

        SplashScreen.this.finish();
    }
    public  void handleActivity(){

        mBackgroundTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {


                int waited = 0;
                // Splash screen pause time
                while (waited < 3500) {
                    sleep(100);
                    waited += 100;
                }

                publishProgress();



                return null;

            }

            @Override
            protected void onProgressUpdate(Object[] values) {

                startActivity();

                super.onProgressUpdate(values);
            }
        };


        mBackgroundTask.execute();
    }

}