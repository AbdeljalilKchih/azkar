package com.abdeljalilkchih.azkar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Fade;
import android.transition.Slide;
import android.view.Gravity;
import android.view.animation.AnticipateOvershootInterpolator;

public class AzkarElMoslemActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_azkar_el_moslem);
        anmation();

    }

    private void anmation() {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Fade fade = new Fade();
            fade.setDuration(700);
            getWindow().setEnterTransition(fade);

            Slide enterTransition = null;
            enterTransition = new Slide();
            enterTransition.setSlideEdge(Gravity.TOP);
            enterTransition.setDuration(700);
            enterTransition.setInterpolator(new AnticipateOvershootInterpolator());
            getWindow().setEnterTransition(enterTransition);


        }

    }
}
