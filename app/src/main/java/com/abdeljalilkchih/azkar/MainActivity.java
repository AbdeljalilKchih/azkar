package com.abdeljalilkchih.azkar;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.transition.Fade;
import android.transition.Slide;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AnticipateOvershootInterpolator;
import android.widget.TextView;

import org.w3c.dom.Text;

import static android.graphics.Typeface.createFromAsset;

public class MainActivity extends AppCompatActivity {
    private TextView TXT0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        anmation();


    }

    public void azkar(View view) {
        Intent MyIntend1 = new Intent(this, AzkarElMoslemActivity.class);
        startActivity(MyIntend1);
    }

    private void anmation() {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Fade fade = new Fade();
            fade.setDuration(700);
            getWindow().setEnterTransition(fade);

            Slide enterTransition = null;
            enterTransition = new Slide();
            enterTransition.setSlideEdge(Gravity.TOP);
            enterTransition.setDuration(700);
            enterTransition.setInterpolator(new AnticipateOvershootInterpolator());
            getWindow().setEnterTransition(enterTransition);


        }
    }
}
